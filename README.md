# Social Network (SocNet)

## About
This is meant to be a simple understandable and extendable framework to provide your own social network. Users will be able to administrate their own profiles and exchange messages and to write posts. It will be a decentraliced system so you can allow your instance to communicate with other instances.

## Contributions
If you are interested in contributing to this project you can fork this and add your features. We may merge them later. I would be happy to hear feedback from you so we can make the whole thing even better.

## Authors
2018 [Marius Timmer](mailto:timmer@mariustimmer.no-ip.org)