<?php

namespace org\SocNet;

use \Exception AS Exception;

class Configuration
{

    const FILENAME = DIRECTORY_SERVERONLY . DIRECTORY_SEPARATOR .'var'. DIRECTORY_SEPARATOR .'configuration.json';
    const LANGUAGE_ENGLISH = 'en';
    const LANGUAGE_GERMAN  = 'de';
    const LANGUAGE_FRANCE  = 'fr';
    const LANGUAGE_RUSSIAN = 'ru';
    const LANGUAGE_CHINESE = 'cn';
    public static $valid_languages = array(
        self::LANGUAGE_ENGLISH,
        self::LANGUAGE_GERMAN,
        self::LANGUAGE_FRANCE,
        self::LANGUAGE_RUSSIAN,
        self::LANGUAGE_CHINESE
    );
    private static $data;

    /**
     * Reads in the configuration from the JSON file.
     */
    private static function provideData()
    {
        try {
            if (!is_readable(self::FILENAME)) {
                throw new Exception('File not readable');
            }
            $raw = file_get_contents(self::FILENAME);
            if ($raw === FALSE) {
                throw new Exception('No content available but the file is readable');
            }
            self::$data = json_decode($raw, TRUE);
            if (!is_array(self::$data)) {
                throw new Exception('Not a valid JSON object');
            }
        } catch (Exception $exception) {
            trigger_error(
                sprintf(
                    'Could not read and parse configuration file "%s": %s',
                    self::FILENAME,
                    $exception->getMessage()
                )
            );
            self::$data = array();
        }
    }

    /**
     * Stores the current configuration to the file.
     */
    private static function storeData()
    {
        try {
            file_put_contents(
                self::FILENAME,
                json_encode(
                    self::$data,
                    JSON_PRETTY_PRINT
                )
            );
        } catch (Exception $exception) {
            trigger_error(
                sprintf(
                    'Could not write configuration to "%s"',
                    self::FILENAME
                )
            );
        }
    }

    /**
     * Reads the requested option from the configuration file.
     * @param string $section
     * @param string $option
     * @return string
     */
    public static function get(string $section, string $option): string
    {
        if (is_null(self::$data)) {
            self::provideData();
        }
        if ((isset(self::$data[$section])) &&
            (isset(self::$data[$section][$option]))) {
            return self::$data[$section][$option];
        }
        return '';
    }

    /**
     * Sets the given value and stores the new configuration
     * @param string $section
     * @param string $option
     * @param string $value
     * @return string The previously value
     */
    public static function set(string $section, string $option, string $value): string
    {
        $old_value = self::get($section, $option);
        if (!isset(self::$data[$section])) {
            self::$data[$section] = array();
        }
        self::$data[$section][$option] = $value;
        self::storeData();
        return $old_value;
    }

}
