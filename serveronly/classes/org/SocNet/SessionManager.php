<?php

namespace org\SocNet;

use \org\SocNet\Database\DTO\User AS User;
use \org\SocNet\Database\DAO\UserDAO AS UserDAO;
use \org\SocNet\Configuration AS Configuration;
use \org\SocNet\Controller\AccountManager AS AccountManager;
use \org\SocNet\Database\DTO\Credential AS Credential;
use \org\SocNet\Database\DAO\CredentialDAO AS CredentialDAO;
use \Exception AS Exception;

class SessionManager
{

    const KEY_USERID       = 'userid';
    const KEY_LOGINTIME    = 'logintime';
    const KEY_SESSIONSTART = 'sessionstart';
    const LOCALE_ENGLISH = 'en_EN.utf8';
    const LOCALE_GERMAN  = 'de_DE.utf8';
    const LOCALE_FRENCH  = 'fr_FR.utf8';
    const LOCALE_CHINESE = 'cn_CN.utf8';
    const LOCALE_RUSSIAN = 'ru_RU.utf8';
    public static $locales = array(
        Configuration::LANGUAGE_ENGLISH => self::LOCALE_ENGLISH,
        Configuration::LANGUAGE_GERMAN  => self::LOCALE_GERMAN,
        Configuration::LANGUAGE_FRANCE  => self::LOCALE_FRENCH,
        Configuration::LANGUAGE_CHINESE => self::LOCALE_CHINESE,
        Configuration::LANGUAGE_RUSSIAN => self::LOCALE_RUSSIAN
    );

    /**
     * Currently logged in user object or null if not fetched already by getCurrentUser().
     * @var User
     */
    private $current_user;

    public function __construct()
    {
        $options = array();
        $this->current_user = null;
        if (!session_start($options)) {
            throw new Exception("Could not start session");
        }
        if (session_status() !== PHP_SESSION_ACTIVE) {
            throw new Exception("New session not available");
        }
        if (empty($this->getValue(self::KEY_SESSIONSTART))) {
            $this->setValue(self::KEY_SESSIONSTART, time());
        }
    }

    /**
     * Returns a value from the session data. This is the safe and
     * prefered way to do this.
     * @param string $key Name of the requested value
     * @return string Value or empty string of not available
     */
    protected function getValue(string $key): string
    {
        if (isset($_SESSION[$key])) {
            return filter_var($_SESSION[$key], FILTER_SANITIZE_SPECIAL_CHARS);
        }
        return '';
    }

    /**
     * Sets a value with the given name in the session data.
     * @param string $key
     * @param string $value
     */
    protected function setValue(string $key, string $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Removes a value with the given name from the session data.
     * @param string $key
     */
    protected function unsetValue(string $key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * Returns the current session ID.
     * @return string Session ID
     */
    public function getSessionID(): string
    {
        return session_id();
    }

    /**
     * Returns the user id of the current user.
     * @return int
     */
    public function getUserID(): int
    {
        return intval($this->getValue(self::KEY_USERID));
    }

    /**
     * Sets the user ID of the current user. Since this value will be used to
     * determinate whether the current user is logged in or not you should use
     * this function only when the user logs in or out.
     * @param int $userid
     */
    public function setUserID(int $userid)
    {
        $this->setValue(self::KEY_USERID, $userid);
    }

    /**
     * Returns whether the current user is logged in or not.
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        return ($this->getUserID() > 0);
    }

    /**
     * Returns the timestamp of the sessionstart.
     * @return int
     */
    public function getSessionStart(): int
    {
        return intval($this->getValue(self::KEY_SESSIONSTART));
    }

    /**
     * Sets up the translations.
     * @param string $user_language
     */
    public static function initLocale(string $user_language)
    {
        $user_locale = self::LOCALE_ENGLISH;
        if (isset(self::$locales[$user_language])) {
            $user_locale = self::$locales[$user_language];
        }
        $textdomain = 'socnet';
        $path = implode(
            DIRECTORY_SEPARATOR,
            array(
                DIRECTORY_SERVERONLY,
                'var',
                'locale'
            )
        );
        setlocale(LC_MESSAGES, $user_locale);
        bindtextdomain($textdomain, $path);
        textdomain($textdomain);
        bind_textdomain_codeset($textdomain, 'UTF-8');
    }

    /**
     * Returns the current user in case he is logged in or null.
     * @return User
     */
    public function getCurrentUser() {
        if (!$this->isLoggedIn()) {
            return null;
        }
        if ($this->current_user === null) {
            $user_id = $this->getUserID();
            $users = User::DAO()->select(
                [
                    UserDAO::COLUMN_USERID => $user_id
                ]
            );
            if (count($users) !== 1) {
                return null;
            }
            $this->current_user = $users[0];
        }
        return $this->current_user;
    }

    /**
     * Checks the login credentials and sets the user id in the
     * SessionManager in case of correct credentials. If they are
     * not correct false will be returned.
     * @param string $username Name of the user
     * @param string $password Plain text password to check
     * @return bool True if the credentials were correct and the user id was set in the Session manager or false
     */
    public function login(string $username, string $password): bool
    {
        $users = User::DAO()->select(
            [
                UserDAO::COLUMN_USERNAME => $username
            ]
        );
        if (count($users) !== 1) {
            return false;
        }
        /* @var $user User */
        $user = $users[0];
        unset($users);
        $credentials = Credential::DAO()->select(
            [
                CredentialDAO::COLUMN_USERID => $user->getUserID()
            ]
        );
        if (count($credentials) !== 1) {
            /**
             * This case should never occur. Every user is meant to have
             * credentials. Users without credentials can not log in.
             * Additionally we write into the errorlog to show something
             * internal is wrong.
             */
            trigger_error(
                sprintf(
                    'User "%s" (#%d) exists but has no credentials',
                    $username,
                    $user->getUserID()
                )
            );
            return false;
        }
        if (AccountManager::verifyPassword($password, $credentials[0]->getPasswordhash()) !== true) {
            // This means the password is just wrong
            return false;
        }
        // Set the user id in the session so the user is logged in now
        $this->setUserID($user->getUserID());
        $this->setValue(self::KEY_LOGINTIME, time());
        return true;
    }

    public function logout() {
        $this->setUserID(0);
        $this->unsetValue(self::KEY_LOGINTIME);
    }

}
