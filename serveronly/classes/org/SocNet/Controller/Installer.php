<?php

namespace org\SocNet\Controller;

use \org\SocNet\Runtime AS Runtime;
use \org\SocNet\Configuration AS Configuration;
use \org\SocNet\Database\PDOConnector AS PDOConnector;

use \Exception AS Exception;

class Installer extends AbstractController
{

    const STATUS_OK = 0;
    const STATUS_INVALID_TITLE = 1;
    const STATUS_INVALID_LANGUAGE = 2;
    const STATUS_INVALID_DBTYPE = 3;
    const STATUS_INVALID_DBFILE = 4;
    const STATUS_INVALID_DBPASSWORDFILE = 5;
    const STATUS_INVALUD_BASEURL = 6;
    const STATUS_INVALUD_ADMIN_USERNAME = 7;
    const STATUS_INVALUD_ADMIN_PASSWORD = 8;
    private $baseurl;
    private $socnettitle;
    private $language;
    private $dbtype;
    private $dbhost;
    private $dbname;
    private $dbuser;
    private $dbpwdfile;
    private $dbfile;
    private $username;
    private $password;

    public function __construct(string $baseurl, string $socnettitle, string $language, string $dbtype, string $dbhost, string $dbname, string $dbuser, string $dbpwdfile, string $dbfile)
    {
        parent::__construct();
        $this->baseurl = $baseurl;
        $this->socnettitle = $socnettitle;
        $this->language = $language;
        $this->dbtype = $dbtype;
        $this->dbhost = $dbhost;
        $this->dbname = $dbname;
        $this->dbuser = $dbuser;
        $this->dbpwdfile = $dbpwdfile;
        $this->dbfile = $dbfile;
    }

    public function setAdminCredentials(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Will validate the given data and return one of the STATUS_* constants
     * @return int
     */
    public function validate(): int
    {
        $socnettitle_length = strlen($this->socnettitle);
        if (($socnettitle_length < 3) ||
            ($socnettitle_length > 50)) {
            return self::STATUS_INVALID_TITLE;
        }
        if (!in_array($this->language, Configuration::$valid_languages)) {
            return self::STATUS_INVALID_LANGUAGE;
        }
        if (!in_array($this->dbtype, PDOConnector::$valid_dbtypes)) {
            return self::STATUS_INVALID_DBTYPE;
        }
        if ($this->dbtype === PDOConnector::DBTYPE_SQLITE) {
            if (!is_readable($this->dbfile)) {
                return self::STATUS_INVALID_DBFILE;
            }
        } else {
            if (!is_readable($this->dbpwdfile)) {
                return self::STATUS_INVALID_DBPASSWORDFILE;
            }
        }
        if (strlen(trim($this->baseurl)) < 7) {
            return self::STATUS_INVALUD_BASEURL;
        }
        if (strlen(trim($this->username)) < 4) {
            return self::STATUS_INVALUD_ADMIN_USERNAME;
        }
        if (!AccountManager::validatePassword($this->password)) {
            return self::STATUS_INVALUD_ADMIN_PASSWORD;
        }
        return self::STATUS_OK;
    }

    /**
     * Saves the new configuration to the configuration file.
     * @return bool Will always return true
     */
    public function save(): bool
    {
        Configuration::set('general', 'baseurl', $this->baseurl);
        Configuration::set('general', 'instancename', $this->socnettitle);
        Configuration::set('general', 'language', $this->language);
        Configuration::set('database', 'type', $this->dbtype);
        switch ($this->dbtype) {
            case PDOConnector::DBTYPE_SQLITE:
                Configuration::set('database', 'file', $this->dbfile);
                break;
            case PDOConnector::DBTYPE_MYSQL:
            case PDOConnector::DBTYPE_POSTGRES:
                Configuration::set('database', 'host', $this->dbhost);
                Configuration::set('database', 'name', $this->dbname);
                Configuration::set('database', 'user', $this->dbuser);
                Configuration::set('database', 'pwdfile', $this->dbpwdfile);
                break;
        }
        return true;
    }

    /**
     * Executes the init statements on the database contained in the init.sql file.
     * @return bool
     */
    public function initializeDatabase(): bool{
        try {
            $filename = 'init.sql';
            if ($this->dbtype === PDOConnector::DBTYPE_SQLITE) {
                $filename = 'init.sqlite.sql';
            }
            $filepath = implode(
                DIRECTORY_SEPARATOR,
                array(
                    DIRECTORY_SERVERONLY,
                    'var',
                    'sql',
                    $filename
                )
            );
            if (!is_readable($filepath)) {
                throw new Exception(
                    sprintf(
                        gettext("Could not read database init script from \"%s\"!"),
                        $filepath
                    )
                );
            }
            $queries = explode(
                ';',
                str_replace(
                    array(
                        "\n",
                        "\r"
                    ),
                    '',
                    file_get_contents(
                        $filepath
                    )
                )
            );
            Runtime::getDatabaseConnector()->beginTransaction();
            foreach ($queries AS $query) {
                if (empty(trim($query))) {
                    continue;
                }
                Runtime::getDatabaseConnector()->exec($query);
            }
            Runtime::getDatabaseConnector()->commit();
            return true;
        } catch (Exception $exception) {
            Runtime::getDatabaseConnector()->rollBack();
            $this->addMessage($exception->getMessage());
            return false;
        }
    }

}