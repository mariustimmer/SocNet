<?php

namespace org\SocNet\Controller;

use \org\SocNet\Runtime AS Runtime;
use \org\SocNet\Configuration AS Configuration;
use \org\SocNet\Database\DTO\Credential AS Credential;
use \org\SocNet\Database\DTO\Usermail AS Usermail;
use \org\SocNet\Database\DTO\Registration AS Registration;
use \org\SocNet\Database\DTO\Profiledata AS Profiledata;
use \org\SocNet\Database\DTO\User AS User;
use \org\SocNet\Database\DAO\CredentialDAO AS CredentialDAO;
use \org\SocNet\Database\DAO\UsermailDAO AS UsermailDAO;
use \org\SocNet\Database\DAO\RegistrationDAO AS RegistrationDAO;
use \org\SocNet\Database\DAO\ProfiledataDAO AS ProfiledataDAO;
use \org\SocNet\Database\DAO\UserDAO AS UserDAO;
use \org\SocNet\Document\VerifyAccountDocument AS VerifyAccountDocument;

use \Exception AS Exception;

class AccountManager extends AbstractController
{

    /**
     * Static object
     * @var AccountManager
     */
    private static $account_manager;

    /**
     * Returns the static account manager or creates and stores a new
     * instance if ther is no instance set already.
     * @return \org\SocNet\Controller\AccountManager
     */
    public static function getInstance(): AccountManager
    {
        if (is_null(self::$account_manager)) {
            self::$account_manager = new AccountManager();
        }
        return self::$account_manager;
    }

    /**
     * Generates a password hash for the given password.
     * @param string $password
     * @return string
     */
    public static function hashPassword(string $password): string
    {
        return password_hash(
            $password,
            PASSWORD_BCRYPT
        );
    }

    /**
     * Wrapper method to verify that a password and a hash belong together.
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public static function verifyPassword(string $password, string $hash): bool {
        return password_verify($password, $hash);
    }

    public function generateVerificationHash(string $username, string $mailaddress): string
    {
        return base64_encode(
            self::hashPassword(
                sprintf(
                    '%s:%s:%s',
                    $username,
                    $mailaddress,
                    date('Y-m-d H:i:s')
                )
            )
        );
    }

    /**
     * Verifies the given verification hash and creates the user, credentials,
     * profiledata and usermail objects in the database before the request
     * itself will be deleted. This procces is secured by an transaction.
     * @param string $code Verification code / hash
     * @return bool True on success or false
     */
    public function verifyCode(string $code): bool
    {
        $found_registrations = Registration::DAO()->select(
            array(
                RegistrationDAO::COLUMN_VERIFICATIONHASH => $code
            )
        );
        if (count($found_registrations) !== 1) {
            return false;
        }
        /* @var $registration Registration */
        $registration = $found_registrations[0];
        try {
            Runtime::getDatabaseConnector()->beginTransaction();
            $user_creation_status = User::DAO()->insert(
                User::DAO()->create(
                    array(
                        UserDAO::COLUMN_USERNAME => $registration->getUsername()
                    )
                )
            );
            if (!$user_creation_status) {
                throw new Exception(
                    gettext("Could not create user")
                );
            }
            $user = User::DAO()->select(
                array(
                    UserDAO::COLUMN_USERNAME => $registration->getUsername()
                )
            );
            if ((!is_array($user)) ||
                (count($user) !== 1)) {
                throw new Exception(
                    gettext("Could not find new user in database")
                );
            }
            /* @var $user User */
            $user = $user[0];
            $credential_creation_status = Credential::DAO()->insert(
                Credential::DAO()->create(
                    array(
                        CredentialDAO::COLUMN_PASSWORDHASH => $registration->getPasswordhash(),
                        CredentialDAO::COLUMN_USERID       => $user->getUserID()
                    )
                )
            );
            if (!$credential_creation_status) {
                throw new Exception(
                    gettext("Could not create user credentials")
                );
            }
            $profiledata_creation_status = Profiledata::DAO()->insert(
                Profiledata::DAO()->create(
                    array(
                        ProfiledataDAO::COLUMN_FIRSTNAME => $registration->getFirstname(),
                        ProfiledataDAO::COLUMN_LASTNAME  => $registration->getLastname(),
                        ProfiledataDAO::COLUMN_USERID    => $user->getUserID()
                    )
                )
            );
            if (!$profiledata_creation_status) {
                throw new Exception(
                    gettext("Could not create user profile")
                );
            }
            $usermail_creation_status = Usermail::DAO()->insert(
                Usermail::DAO()->create(
                    array(
                        UsermailDAO::COLUMN_MAILADDRESS => $registration->getMailaddress(),
                        UsermailDAO::COLUMN_USERID      => $user->getUserID()
                    )
                )
            );
            if (!$usermail_creation_status) {
                throw new Exception(
                    gettext("Could not create user mail data")
                );
            }
            $reg_delete_status = Registration::DAO()->delete($registration);
            if (!$reg_delete_status) {
                throw new Exception(
                    gettext("Could not delete the account creation request")
                );
            }
            Runtime::getDatabaseConnector()->commit();
            return true;
        } catch (Exception $exception) {
            $this->addMessage($exception->getMessage());
            Runtime::getDatabaseConnector()->rollBack();
            return false;
        }
    }

    public function signUp(string $username, string $password, string $mailaddress, string $firstname, string $lastname): bool
    {
        $user = User::DAO()->select(
            array(
                UserDAO::COLUMN_USERNAME => $username
            )
        );
        if (count($user) > 0) {
            $this->addMessage(
                sprintf(
                    'The username "%s" is already used by another account.',
                    $username
                )
            );
            return false;
        }
        if (!self::validatePassword($password)) {
            $this->addMessage(
                gettext("A valid password must be at least 8 characters long and contain a number")
            );
            return false;
        }
        $registrations_username = Registration::DAO()->select(
            array(
                RegistrationDAO::COLUMN_USERNAME => $username
            )
        );
        if (count($registrations_username) > 0) {
            $this->addMessage(
                sprintf(
                    'The username "%s" is already pending for registration.',
                    $username
                )
            );
            return false;
        }
        $found_mailaddress = Usermail::DAO()->select(
            array(
                UsermailDAO::COLUMN_MAILADDRESS => $mailaddress
            )
        );
        if (count($found_mailaddress) > 0) {
            $this->addMessage(
                sprintf(
                    'The email address "%s" is already used.',
                    $mailaddress
                )
            );
            return false;
        }
        $verificationhash = $this->generateVerificationHash(
            $username,
            $mailaddress
        );
        $savestatus = Registration::DAO()->insert(
            Registration::DAO()->create(
                array(
                    RegistrationDAO::COLUMN_USERNAME         => $username,
                    RegistrationDAO::COLUMN_PASSWORDHASH     => self::hashPassword($password),
                    RegistrationDAO::COLUMN_MAILADDRESS      => $mailaddress,
                    RegistrationDAO::COLUMN_FIRSTNAME        => $firstname,
                    RegistrationDAO::COLUMN_LASTNAME         => $lastname,
                    RegistrationDAO::COLUMN_VERIFICATIONHASH => $verificationhash
                )
            )
        );
        if (!$savestatus) {
            throw new Exception(
                gettext("Could not store the account verification!")
            );
            return false;
        }
        $url = sprintf(
            '%s/register/verify.php?%s=%s',
            Configuration::get('general', 'full_url'),
            VerifyAccountDocument::KEY_VERIFYCODE,
            $verificationhash
        );
        $message = sprintf(
            gettext("Hello %s %s"),
            $firstname,
            $lastname
        ) .",\r\n\r\n";
        $message .= sprintf(
            gettext("to activate your new account you have to visit the following Link: %s"),
            $url
        ) ."\n\r\n\r";
        $message .= sprintf(
            gettext("Greetings, %s"),
            Configuration::get('general', 'instancename')
        ) ."\n\r";
        $headers = array(
            'MIME-Version: 1.0',
            'Content-Type: text/plain; Charset=UTF-8'
        );
        if ((!empty(Configuration::get('general', 'instancename'))) &&
            (!empty(Configuration::get('general', 'mail')))) {
            array_push(
                $headers,
                sprintf(
                    'From: %s <%s>',
                    Configuration::get('general', 'instancename'),
                    Configuration::get('general', 'mail')
                )
            );
        }
        $mailstatus = mail(
            sprintf('%s %s <%s>', $firstname, $lastname, $mailaddress),
            '=?UTF-8?B?'. base64_encode(gettext("New account verification")) .'?=',
            $message,
            implode("\r\n", $headers)
        );
        return $mailstatus;
    }

    /**
     * Validates a password depending on the lenght and complexity.
     * @param string $password The password to validate
     * @return bool True if the password is valid or false
     */
    public static function validatePassword(string $password): bool
    {
        if (mb_strlen($password) < 8) {
            // A valid password has to be longer than 3 characters
            return false;
        }
        if (1 !== preg_match('~[0-9]~', $password)) {
            // There should be at least one numeric character
            return false;
        }
        return true;
    }

}
