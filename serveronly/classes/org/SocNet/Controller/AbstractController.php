<?php

namespace org\SocNet\Controller;

abstract class AbstractController
{

    /**
     * Array containing all occured errors.
     * @var array
     */
    private $messages;

    public function __construct()
    {
        $this->messages = array();
    }

    /**
     * Stores a message to be printed out to the user or other stuff like later.
     * @param string $message Message to store
     */
    protected function addMessage(string $message)
    {
        array_push(
            $this->messages,
            $message
        );
    }

    /**
     * Returns all messages the controller added.
     * @return array Messages
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

}
