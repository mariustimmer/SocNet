<?php

namespace org\SocNet\Database;

use \org\SocNet\Configuration AS Configuration;
use \Exception AS Exception;
use \PDO AS PDO;
use \PDOException AS PDOException;

class PDOConnector
{

    const DBTYPE_SQLITE = 'sqlite';
    const DBTYPE_MYSQL = 'mysql';
    const DBTYPE_POSTGRES = 'postgres';
    public static $valid_dbtypes = array(
        self::DBTYPE_SQLITE,
        self::DBTYPE_MYSQL,
        self::DBTYPE_POSTGRES
    );

    /**
     * PDO connection to the database
     * @var PDO
     */
    private $connection;

    public function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        try {
            $username = null;
            $password = null;
            $dsn = null;
            if (Configuration::get('database', 'type') === self::DBTYPE_SQLITE) {
                $dsn = sprintf(
                    'sqlite:%s',
                    Configuration::get('database', 'file')
                );
            } else {
                $dsn = sprintf(
                    '%s:host=%s;dbname=%s',
                    Configuration::get('database', 'type'),
                    Configuration::get('database', 'host'),
                    Configuration::get('database', 'name')
                );
                $username = Configuration::get('database', 'user');
                $password = file_get_contents(Configuration::get('database', 'pwdfile'));
            }
            if ((empty($username)) && (empty($password))) {
                $this->connection = new PDO($dsn);
            } else {
                $this->connection = new PDO($dsn, $username, $password);
            }
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $exception) {
            trigger_error($exception->getMessage());
            throw new Exception('Could not connect to database');
        }
    }

    public function beginTransaction()
    {
        return $this->connection->beginTransaction();
    }

    public function commit()
    {
        return $this->connection->commit();
    }

    public function rollBack()
    {
        return $this->connection->rollBack();
    }

    public function inTransaction()
    {
        return $this->connection->inTransaction();
    }

    public function exec(string $query)
    {
        return $this->connection->exec($query);
    }

    public function getConnector(): PDO
    {
        return $this->connection;
    }

}
