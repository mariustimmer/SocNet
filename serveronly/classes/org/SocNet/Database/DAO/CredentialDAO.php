<?php

namespace org\SocNet\Database\DAO;

use \org\SocNet\Database\DTO\Credential AS Credential;
use \Exception AS Exception;

class CredentialDAO extends AbstractDAO
{

    const TABLE_NAME = 'credentials';
    const COLUMN_CREDENTIALID = 'credentialid';
    const COLUMN_USERID       = 'userid';
    const COLUMN_PASSWORDHASH = 'passwordhash';
    const COLUMN_CREATED      = 'created';

    public function __construct()
    {
        parent::__construct(
            self::TABLE_NAME,
            array(
                self::COLUMN_CREDENTIALID,
                self::COLUMN_USERID,
                self::COLUMN_PASSWORDHASH,
                self::COLUMN_CREATED
            ),
            array(
                self::COLUMN_CREDENTIALID
            )
        );
    }

    /**
     * @param array $data
     * @return \org\SocNet\Database\DTO\Credential
     * @throws Exception
     */
    public function create(array $data)
    {
        $credential = new Credential();
        foreach ($data AS $key => $value) {
            switch ($key) {
                case self::COLUMN_CREDENTIALID:
                    throw new Exception(
                        sprintf(
                            gettext("Attribute '%s' can not be set by hand"),
                            $key
                        )
                    );
                    break;
                case self::COLUMN_USERID:
                    $credential->setUserID($value);
                    break;
                case self::COLUMN_PASSWORDHASH:
                    $credential->setPasswordhash($value);
                    break;
            }
        }
        $credential->setCreated(date('Y-m-d H:i:s'));
        return $credential;
    }

    /**
     * @param \org\SocNet\Database\DTO\Credential $dto
     * @return array
     * @throws Exception
     */
    public function toArray($dto): array
    {
        if (!$dto instanceof Credential) {
            throw new Exception('dto has to be Credential');
        }
        return array(
            self::COLUMN_CREDENTIALID => $dto->getCredentialID(),
            self::COLUMN_USERID       => $dto->getUserID(),
            self::COLUMN_PASSWORDHASH => $dto->getPasswordhash(),
            self::COLUMN_CREATED      => $dto->getCreated()
        );
    }

}
