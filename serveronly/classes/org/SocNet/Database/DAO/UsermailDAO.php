<?php

namespace org\SocNet\Database\DAO;

use \org\SocNet\Database\DTO\Usermail AS Usermail;
use \Exception AS Exception;

class UsermailDAO extends AbstractDAO
{

    const TABLE_NAME = 'usermails';
    const COLUMN_USERMAILID  = 'usermailid';
    const COLUMN_USERID      = 'userid';
    const COLUMN_MAILADDRESS = 'mailaddress';
    const COLUMN_CREATED     = 'created';

    public function __construct()
    {
        parent::__construct(
            self::TABLE_NAME,
            array(
                self::COLUMN_USERMAILID,
                self::COLUMN_USERID,
                self::COLUMN_MAILADDRESS,
                self::COLUMN_CREATED
            ),
            array(
                self::COLUMN_USERMAILID
            )
        );
    }

    /**
     * @param array $data
     * @return \org\SocNet\Database\DTO\Usermail
     * @throws Exception
     */
    public function create(array $data)
    {
        $usermail = new Usermail();
        foreach ($data AS $key => $value) {
            switch ($key) {
                case self::COLUMN_USERMAILID:
                    throw new Exception(
                        sprintf(
                            gettext("Attribute '%s' can not be set by hand"),
                            $key
                        )
                    );
                    break;
                case self::COLUMN_USERID:
                    $usermail->setUserID($value);
                    break;
                case self::COLUMN_MAILADDRESS:
                    $usermail->setMailaddress($value);
                    break;
            }
        }
        $usermail->setCreated(date('Y-m-d H:i:s'));
        return $usermail;
    }

    /**
     * @param \org\SocNet\Database\DTO\Usermail $dto
     * @return array
     * @throws Exception
     */
    public function toArray($dto): array
    {
        if (!$dto instanceof Usermail) {
            throw new Exception('dto has to be Usermail');
        }
        return array(
            self::COLUMN_USERMAILID  => $dto->getUsermailID(),
            self::COLUMN_USERID      => $dto->getUserID(),
            self::COLUMN_MAILADDRESS => $dto->getMailaddress(),
            self::COLUMN_CREATED     => $dto->getCreated()
        );
    }

}
