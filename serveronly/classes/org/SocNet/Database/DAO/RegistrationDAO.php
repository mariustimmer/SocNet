<?php

namespace org\SocNet\Database\DAO;

use \org\SocNet\Database\DTO\Registration AS Registration;
use \Exception AS Exception;

class RegistrationDAO extends AbstractDAO
{

    const TABLE_NAME = 'registrations';
    const COLUMN_REGISTRATIONID   = 'regid';
    const COLUMN_USERNAME         = 'username';
    const COLUMN_FIRSTNAME        = 'firstname';
    const COLUMN_LASTNAME         = 'lastname';
    const COLUMN_MAILADDRESS      = 'mailaddress';
    const COLUMN_PASSWORDHASH     = 'passwordhash';
    const COLUMN_VERIFICATIONHASH = 'verificationhash';
    const COLUMN_CREATED          = 'created';

    public function __construct()
    {
        parent::__construct(
            self::TABLE_NAME,
            array(
                self::COLUMN_REGISTRATIONID,
                self::COLUMN_USERNAME,
                self::COLUMN_FIRSTNAME,
                self::COLUMN_LASTNAME,
                self::COLUMN_MAILADDRESS,
                self::COLUMN_PASSWORDHASH,
                self::COLUMN_VERIFICATIONHASH,
                self::COLUMN_CREATED
            ),
            array(
                self::COLUMN_REGISTRATIONID
            )
        );
    }

    /**
     * @param array $data
     * @return \org\SocNet\Database\DTO\Registration
     * @throws Exception
     */
    public function create(array $data)
    {
        $registration = new Registration();
        foreach ($data AS $key => $value) {
            switch ($key) {
                case self::COLUMN_REGISTRATIONID:
                    throw new Exception(
                        sprintf(
                            gettext("Attribute '%s' can not be set by hand"),
                            $key
                        )
                    );
                    break;
                case self::COLUMN_USERNAME:
                    $registration->setUsername($value);
                    break;
                case self::COLUMN_FIRSTNAME:
                    $registration->setFirstname($value);
                    break;
                case self::COLUMN_LASTNAME:
                    $registration->setLastname($value);
                    break;
                case self::COLUMN_MAILADDRESS:
                    $registration->setMailaddress($value);
                    break;
                case self::COLUMN_PASSWORDHASH:
                    $registration->setPasswordhash($value);
                    break;
                case self::COLUMN_VERIFICATIONHASH:
                    $registration->setVerificationhash($value);
                    break;
            }
        }
        $registration->setCreated(date('Y-m-d H:i:s'));
        return $registration;
    }

    /**
     * @param \org\SocNet\Database\DTO\Registration $dto
     * @return array
     * @throws Exception
     */
    public function toArray($dto): array
    {
        if (!$dto instanceof Registration) {
            throw new Exception('dto has to be Registration');
        }
        return array(
            self::COLUMN_REGISTRATIONID   => $dto->getRegID(),
            self::COLUMN_USERNAME         => $dto->getUsername(),
            self::COLUMN_FIRSTNAME        => $dto->getFirstname(),
            self::COLUMN_LASTNAME         => $dto->getLastname(),
            self::COLUMN_MAILADDRESS      => $dto->getMailaddress(),
            self::COLUMN_PASSWORDHASH     => $dto->getPasswordhash(),
            self::COLUMN_VERIFICATIONHASH => $dto->getVerificationhash(),
            self::COLUMN_CREATED          => $dto->getCreated()
        );
    }

}
