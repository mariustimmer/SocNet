<?php

namespace org\SocNet\Database\DAO;

use \org\SocNet\Database\DTO\User AS User;
use \Exception AS Exception;

class UserDAO extends AbstractDAO
{

    const TABLE_NAME = 'users';
    const COLUMN_USERID   = 'userid';
    const COLUMN_USERNAME = 'username';
    const COLUMN_CREATED  = 'created';

    public function __construct()
    {
        parent::__construct(
            self::TABLE_NAME,
            array(
                self::COLUMN_USERID,
                self::COLUMN_USERNAME,
                self::COLUMN_CREATED
            ),
            array(
                self::COLUMN_USERID
            )
        );
    }

    /**
     * @param User $data
     */
    public function create(array $data)
    {
        $user = new User();
        $user->setUsername($data[self::COLUMN_USERNAME]);
        $user->setCreated(date('Y-m-d H:i:s'));
        return $user;
    }

    /**
     * 
     * @param User $dto
     */
    public function toArray($dto): array
    {
        if (!$dto instanceof User) {
            throw new Exception(
                sprintf(
                    '%s is not instance of User',
                    get_class($dto)
                )
            );
        }
        return array(
            self::COLUMN_USERID   => $dto->getUserID(),
            self::COLUMN_USERNAME => $dto->getUsername(),
            self::COLUMN_CREATED  => $dto->getCreated()
        );
    }

}
