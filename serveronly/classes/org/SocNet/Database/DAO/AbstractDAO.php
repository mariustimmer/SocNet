<?php

namespace org\SocNet\Database\DAO;

use \org\SocNet\Database\DTO\DTO AS DTO;
use \org\SocNet\Runtime AS Runtime;
use \PDO AS PDO;
use \PDOException AS PDOException;

abstract class AbstractDAO
{

    /**
     * Table name
     * @var string
     */
    private $tablename;

    /**
     * Names of the columns
     * @var array
     */
    private $columns;

    /**
     * List of primary keys
     * @var array
     */
    private $primary_keys;

    protected function __construct(string $tablename, array $columns, array $primary_keys = array())
    {
        $this->tablename = $tablename;
        $this->columns = $columns;
        $this->primary_keys = $primary_keys;
    }

    public function getTableName(): string
    {
        return $this->tablename;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function getPrimaryKeys(): array
    {
        return $this->primary_keys;
    }

    public function getDTOClassName(): string
    {
        return str_replace(
            'DAO',
            '',
            str_replace(
                chr(92). 'DAO'. chr(92),
                chr(92). 'DTO'. chr(92),
                get_called_class()
            )
        );
    }

    public function insert(DTO $dto): bool
    {
        $bindings = array();
        $columns = array();
        $data = $this->toArray($dto);
        asort($data);
        foreach ($data AS $key => $value) {
            if (in_array($key, $this->getPrimaryKeys()))
                continue;
            $bindings[':'. $key] = $value;
            $columns[] = $key;
        }
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $this->getTableName(),
            implode(', ', $columns),
            implode(', ', array_keys($bindings))
        );
        try {
            $statement = Runtime::getDatabaseConnector()->getConnector()->prepare($query);
            foreach ($bindings AS $key => &$value) {
                $statement->bindParam($key, $value);
            }
            return $statement->execute();
        } catch (PDOException $exception) {
            trigger_error($exception->getMessage());
            return false;
        }
    }

    public function update(DTO $dto): bool
    {
        $bindings = array();
        $data_query = array();
        $where_query = array();
        $data = $this->toArray($dto);
        asort($data);
        foreach ($data AS $key => $value) {
            $query_part = $key .' = :'. $key;
            $bindings[':'. $key] = $value;
            if (in_array($key, $this->getPrimaryKeys())) {
                // Primary key for the WHERE part
                array_push(
                    $where_query,
                    $query_part
                );
            } else {
                // Data for the SET part
                array_push(
                    $data_query,
                    $query_part
                );
            }
        }
        $query = sprintf(
            'UPDATE %s SET %s WHERE %s',
            $this->getTableName(),
            implode(
                ', ',
                $data_query
            ),
            implode(
                ' AND ',
                $where_query
            )
        );
        try {
            $statement = Runtime::getDatabaseConnector()->getConnector()->prepare($query);
            foreach ($bindings AS $key => &$value) {
                $statement->bindParam($key, $value);
            }
            return $statement->execute();
        } catch (PDOException $exception) {
            trigger_error($exception->getMessage());
            return false;
        }
    }

    public function delete(DTO $dto): bool
    {
        $bindings = array();
        $where_query = array();
        $data = $this->toArray($dto);
        asort($data);
        foreach ($data AS $key => $value) {
            if (!in_array($key, $this->getPrimaryKeys())) {
                continue;
            }
            array_push(
                $where_query,
                sprintf(
                    '%s = :%s',
                    $key,
                    $key
                )
            );
            $bindings[':'. $key] = $value;
        }
        $query = sprintf(
            'DELETE FROM %s WHERE %s',
            $this->getTableName(),
            implode(
                ' AND ',
                $where_query
            )
        );
        try {
            $statement = Runtime::getDatabaseConnector()->getConnector()->prepare($query);
            foreach ($bindings AS $key => &$value) {
                $statement->bindParam($key, $value);
            }
            return $statement->execute();
        } catch (PDOException $exception) {
            trigger_error($exception->getMessage());
            return false;
        }
    }

    public function select(array $data)
    {
        $query = sprintf(
            'SELECT %s FROM %s',
            implode(', ', $this->getColumns()),
            $this->getTableName()
        );
        if (count($data) > 0) {
            $bindings = array();
            $where_condition = '';
            foreach ($data AS $column => $value) {
                if (!empty($where_condition)) {
                    $where_condition .= ' AND';
                }
                $where_condition .= sprintf(
                    ' %s = :%s',
                    $column,
                    $column
                );
                $bindings[':'. $column] = $value;
            }
            $query .= ' WHERE'. $where_condition;
        }
        try {
            $dbconnector = Runtime::getDatabaseConnector()->getConnector();
            $statement = $dbconnector->prepare($query);
            foreach ($bindings AS $key => &$value) {
                $statement->bindParam($key, $value);
            }
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_CLASS, $this->getDTOClassName());
        } catch (PDOException $exception) {
            trigger_error($exception->getMessage());
            throw new Exception('Could not execute select query');
        }
    }

    abstract public function toArray($dto): array;
    abstract public function create(array $data);

}
