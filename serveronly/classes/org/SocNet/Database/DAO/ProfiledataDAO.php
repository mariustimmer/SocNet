<?php

namespace org\SocNet\Database\DAO;

use \org\SocNet\Database\DTO\Profiledata AS Profiledata;
use \Exception AS Exception;

class ProfiledataDAO extends AbstractDAO
{

    const TABLE_NAME = 'profiledata';
    const COLUMN_PROFILEID  = 'profileid';
    const COLUMN_USERID     = 'userid';
    const COLUMN_FIRSTNAME  = 'firstname';
    const COLUMN_LASTNAME   = 'lastname';
    const COLUMN_GENDER     = 'gender';
    const COLUMN_DAYOFBIRTH = 'dayofbirth';
    const COLUMN_CREATED    = 'created';

    public function __construct()
    {
        parent::__construct(
            self::TABLE_NAME,
            array(
                self::COLUMN_PROFILEID,
                self::COLUMN_USERID,
                self::COLUMN_FIRSTNAME,
                self::COLUMN_LASTNAME,
                self::COLUMN_GENDER,
                self::COLUMN_DAYOFBIRTH,
                self::COLUMN_CREATED
            ),
            array(
                self::COLUMN_PROFILEID
            )
        );
    }

    /**
     * @param array $data
     * @return \org\SocNet\Database\DTO\Profiledata
     * @throws Exception
     */
    public function create(array $data)
    {
        $profiledata = new Profiledata();
        foreach ($data AS $key => $value) {
            switch ($key) {
                case self::COLUMN_PROFILEID:
                    throw new Exception(
                        sprintf(
                            gettext("Attribute '%s' can not be set by hand"),
                            $key
                        )
                    );
                    break;
                case self::COLUMN_USERID:
                    $profiledata->setUserID($value);
                    break;
                case self::COLUMN_FIRSTNAME:
                    $profiledata->setFirstname($value);
                    break;
                case self::COLUMN_LASTNAME:
                    $profiledata->setLastname($value);
                    break;
                case self::COLUMN_GENDER:
                    $profiledata->setGender($value);
                    break;
                case self::COLUMN_DAYOFBIRTH:
                    $profiledata->setDayofbirth($value);
                    break;
            }
        }
        $profiledata->setCreated(date('Y-m-d H:i:s'));
        return $profiledata;
    }

    /**
     * @param \org\SocNet\Database\DTO\Profiledata $dto
     * @return array
     * @throws Exception
     */
    public function toArray($dto): array
    {
        if (!$dto instanceof Profiledata) {
            throw new Exception('dto has to be Profiledata');
        }
        return array(
            self::COLUMN_PROFILEID  => $dto->getProfileID(),
            self::COLUMN_USERID     => $dto->getUserID(),
            self::COLUMN_FIRSTNAME  => $dto->getFirstname(),
            self::COLUMN_LASTNAME   => $dto->getLastname(),
            self::COLUMN_GENDER     => $dto->getGender(),
            self::COLUMN_DAYOFBIRTH => $dto->getDayofbirth(),
            self::COLUMN_CREATED    => $dto->getCreated()
        );
    }

}
