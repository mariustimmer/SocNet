<?php

namespace org\SocNet\Database\DTO;

class Profiledata extends DTO
{

    private $profileid;
    private $userid;
    private $firstname;
    private $lastname;
    private $gender;
    private $dayofbirth;
    private $created;

    function getProfileID()
    {
        return $this->profileid;
    }

    function getUserID()
    {
        return $this->userid;
    }

    function getFirstname()
    {
        return $this->firstname;
    }

    function getLastname()
    {
        return $this->lastname;
    }

    function getGender()
    {
        return $this->gender;
    }

    function getDayofbirth()
    {
        return $this->dayofbirth;
    }

    function getCreated()
    {
        return $this->created;
    }

    function setProfileID($profileid)
    {
        $this->profileid = $profileid;
    }

    function setUserID($userid)
    {
        $this->userid = $userid;
    }

    function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    function setGender($gender)
    {
        $this->gender = $gender;
    }

    function setDayofbirth($dayofbirth)
    {
        $this->dayofbirth = $dayofbirth;
    }

    function setCreated($created)
    {
        $this->created = $created;
    }

}
