<?php

namespace org\SocNet\Database\DTO;

class Registration extends DTO
{

    private $regid;
    private $username;
    private $firstname;
    private $lastname;
    private $mailaddress;
    private $passwordhash;
    private $verificationhash;
    private $created;

    function getRegID()
    {
        return $this->regid;
    }

    function getUsername()
    {
        return $this->username;
    }

    function getFirstname()
    {
        return $this->firstname;
    }

    function getLastname()
    {
        return $this->lastname;
    }

    function getMailaddress()
    {
        return $this->mailaddress;
    }

    function getPasswordhash()
    {
        return $this->passwordhash;
    }

    function getVerificationhash()
    {
        return $this->verificationhash;
    }

    function getCreated()
    {
        return $this->created;
    }

    function setRegID($regid)
    {
        $this->regid = $regid;
    }

    function setUsername($username)
    {
        $this->username = $username;
    }

    function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    function setMailaddress($mailaddress)
    {
        $this->mailaddress = $mailaddress;
    }

    function setPasswordhash($passwordhash)
    {
        $this->passwordhash = $passwordhash;
    }

    function setVerificationhash($verificationhash)
    {
        $this->verificationhash = $verificationhash;
    }

    function setCreated($created)
    {
        $this->created = $created;
    }

}
