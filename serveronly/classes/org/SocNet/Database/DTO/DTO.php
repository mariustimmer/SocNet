<?php

namespace org\SocNet\Database\DTO;

abstract class DTO
{

    /**
     * Stores the static DAO classes
     * @var array
     */
    private static $daos;

    /**
     * Returns the DAO object according to this DTO.
     * @return \org\SocNet\Database\DAO\AbstractDAO
     */
    public static function DAO()
    {
        $class_name = str_replace('DTO', 'DAO', get_called_class()) .'DAO';
        if (!is_array(self::$daos)) {
            self::$daos = array();
        }
        if (!isset(self::$daos[$class_name])) {
            self::$daos[$class_name] = new $class_name();
        }
        return self::$daos[$class_name];
    }

}
