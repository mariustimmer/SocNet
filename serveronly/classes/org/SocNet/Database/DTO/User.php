<?php

namespace org\SocNet\Database\DTO;

class User extends DTO
{

    private $userid;
    private $username;
    private $created;

    function getUserID()
    {
        return $this->userid;
    }

    function getUsername()
    {
        return $this->username;
    }

    function getCreated()
    {
        return $this->created;
    }

    function setUserID($userid)
    {
        $this->userid = $userid;
    }

    function setUsername($username)
    {
        $this->username = $username;
    }

    function setCreated($created)
    {
        $this->created = $created;
    }

}
