<?php

namespace org\SocNet\Database\DTO;

class Credential extends DTO
{

    private $credentialid;
    private $userid;
    private $passwordhash;
    private $created;

    function getCredentialID()
    {
        return $this->credentialid;
    }

    function getUserID()
    {
        return $this->userid;
    }

    function getPasswordhash()
    {
        return $this->passwordhash;
    }

    function getCreated()
    {
        return $this->created;
    }

    function setCredentialID($credentialid)
    {
        $this->credentialid = $credentialid;
    }

    function setUserID($userid)
    {
        $this->userid = $userid;
    }

    function setPasswordhash($passwordhash)
    {
        $this->passwordhash = $passwordhash;
    }

    function setCreated($created)
    {
        $this->created = $created;
    }

}
