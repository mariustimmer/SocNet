<?php

namespace org\SocNet\Database\DTO;

class Usermail extends DTO
{

    private $usermailid;
    private $userid;
    private $mailaddress;
    private $created;

    function getUsermailID()
    {
        return $this->usermailid;
    }

    function getUserID()
    {
        return $this->userid;
    }

    function getMailaddress()
    {
        return $this->mailaddress;
    }

    function getCreated()
    {
        return $this->created;
    }

    function setUsermailID($usermailid)
    {
        $this->usermailid = $usermailid;
    }

    function setUserID($userid)
    {
        $this->userid = $userid;
    }

    function setMailaddress($mailaddress)
    {
        $this->mailaddress = $mailaddress;
    }

    function setCreated($created)
    {
        $this->created = $created;
    }

}
