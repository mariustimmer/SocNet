<?php

namespace org\SocNet\Document;

use \org\SocNet\Runtime AS Runtime;

abstract class RestrictedDocument extends AbstractDocument
{

    public function __construct(string $templatename)
    {
        parent::__construct($templatename);
        if (!Runtime::getSessionManager()->isLoggedIn()) {
            $this->setTemplateName('general.html');
            $this->addSpecialContent('TITLE', gettext("Restricted document"));
            $this->addSpecialContent('SUBTITLE', gettext("This document is restricted"));
            $this->addSpecialContent('TEXT', gettext("You are not allowed to access this document. You need to log in first in order to see the contents of this document."));
        }
    }

}
