<?php

namespace org\SocNet\Document;

use \org\SocNet\Controller\AccountManager AS AccountManager;
use \org\SocNet\Document\Element\AlertMessage AS AlertMessage;

class VerifyAccountDocument extends AbstractDocument
{

    const KEY_VERIFYCODE = 'verificationcode';
    const KEY_SUBMIT = 'submitverification';

    public function __construct()
    {
        parent::__construct('verifyaccount.html');
        $this->setTitle(gettext("Account verification"));
    }

    public function execute()
    {
        $messageelement = '';
        if ((isset($_POST[self::KEY_SUBMIT])) &&
            (isset($_POST[self::KEY_VERIFYCODE]))) {
            $verify_code = $this->getValue(self::KEY_VERIFYCODE);
            $success = AccountManager::getInstance()->verifyCode($verify_code);
            if ($success) {
                $messageelement = new AlertMessage(
                    gettext("Your account has been verified successfully. You should be able to log in now."),
                    AlertMessage::PRIORITY_SUCCESS
                );
            } else {
                $messages = AccountManager::getInstance()->getMessages();
                $reason = array_pop($messages);
                $messageelement = new AlertMessage(
                    sprintf(
                        gettext("Your verification code could not be verified: Please check your code.<!-- Details: %s -->"),
                        $reason
                    ),
                    AlertMessage::PRIORITY_WARNING
                );
            }
        }
        $this->addSpecialContent(
            'EXECUTION_MESSAGE',
            $messageelement
        );
    }

    public function setup()
    {
        $content = array(
            'VERIFY'         => gettext("Verification"),
            'VERIFY_LONG'    => gettext("Verify your new account"),
            'VERIFY_TEXT'    => gettext("After you have used the sign up formular you should have received an mail containing your personal verification code. All you need to do is to enter this code into the following field and hit the submit button to finish the proccess of creating your new account."),
            'VERIFYCODE'     => gettext("Verification code"),
            'VERIFY_NOTE'    => gettext("Insert your verification code from your mail into this field"),
            'SUBMIT'         => gettext("Submit"),
            'KEY_VERIFYCODE' => self::KEY_VERIFYCODE,
            'KEY_SUBMIT'     => self::KEY_SUBMIT,
            'CODE'           => $this->getValue(self::KEY_VERIFYCODE, self::GET) ?: ''
        );
        foreach ($content AS $key => $value) {
            $this->addSpecialContent($key, $value);
        }
    }

}
