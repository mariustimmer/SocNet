<?php

namespace org\SocNet\Document;

use \org\SocNet\Document\AbstractDocument AS AbstractDocument;

class RegisterDocument extends AbstractDocument
{

    const KEY_SUBMIT = 'signup';
    const KEY_FIRSTNAME = 'firstname';
    const KEY_LASTNAME = 'lastname';
    const KEY_USERNAME = 'username';
    const KEY_EMAIL = 'email';
    const KEY_EMAIL_CONFIRM = 'email_confirm';
    const KEY_PASSWORD = 'password';
    const KEY_TAC = 'tac';

    public function __construct()
    {
        parent::__construct('register.html');
        $this->setTitle(gettext("Register"));
    }

    public function setup()
    {
        $this->addSpecialContent('REGISTER', gettext("Sign up"));
        $this->addSpecialContent('REGISTER_LONG', gettext("Create a new account"));
        $this->addSpecialContent('REGISTER_TEXT', gettext("To create a new account here you need to fill in the following formular fields. After that we will send you a confirmation mail with a link to activate your new account."));
        $this->addSpecialContent('FIRSTNAME', gettext("First name"));
        $this->addSpecialContent('KEY_FIRSTNAME', self::KEY_FIRSTNAME);
        $this->addSpecialContent('LASTNAME', gettext("Last name"));
        $this->addSpecialContent('KEY_LASTNAME', self::KEY_LASTNAME);
        $this->addSpecialContent('USERNAME', gettext("Username"));
        $this->addSpecialContent('KEY_USERNAME', self::KEY_USERNAME);
        $this->addSpecialContent('EMAIL', gettext("Mail address"));
        $this->addSpecialContent('KEY_EMAIL', self::KEY_EMAIL);
        $this->addSpecialContent('EMAIL_CONFIRM', gettext("Confirm mail address"));
        $this->addSpecialContent('KEY_EMAIL_CONFIRM', self::KEY_EMAIL_CONFIRM);
        $this->addSpecialContent('PASSWORD', gettext("Password"));
        $this->addSpecialContent('KEY_PASSWORD', self::KEY_PASSWORD);
        $this->addSpecialContent('AGREEMENT', gettext("I agree to the terms and conditions"));
        $this->addSpecialContent('KEY_AGREEMENT', self::KEY_TAC);
        $this->addSpecialContent('SUBMIT', gettext("Submit"));
        $this->addSpecialContent('KEY_SUBMIT', self::KEY_SUBMIT);
        $this->addSpecialContent('USERNAME_NOTE', gettext("Choose a username with a length between 8 and 32 characters"));
        $this->addSpecialContent('PASSWORD_NOTE', gettext("Your password has to be at least 8 characters long"));
    }

    public function execute()
    {
    }

}
