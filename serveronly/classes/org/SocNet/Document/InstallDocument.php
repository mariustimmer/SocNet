<?php

namespace org\SocNet\Document;

use \org\SocNet\Database\PDOConnector AS PDOConnector;
use \org\SocNet\Document\Element\AlertMessage AS AlertMessage;
use \org\SocNet\Controller\AccountManager AS AccountManager;
use \org\SocNet\Controller\Installer AS Installer;

class InstallDocument extends AbstractDocument
{

    const MODE_FORMULAR = 'formular';
    const MODE_SAVE     = 'save';
    const KEY_SUBMIT = 'install_submit';
    private $mode;

    /**
     * String to store occuring messages (including HTML)
     * @var string
     */
    private $messages;

    public function __construct()
    {
        parent::__construct('install.html');
        $this->mode = self::MODE_FORMULAR;
        $this->messages = '';
        $this->setTitle(gettext("Installation"));
    }

    public function execute()
    {
        if ($this->mode === self::MODE_SAVE) {
            /**
             * This is the place we need to store the information within
             * the configuration before we set up the database.
             */
            $baseurl = $this->getValue('baseurl');
            $socnettitle = $this->getValue('socnettitle');
            $language = $this->getValue('language');
            $dbtype = $this->getValue('dbtype');
            $dbhost = $this->getValue('dbhost');
            $dbname = $this->getValue('dbname');
            $dbuser = $this->getValue('dbuser');
            $dbpwdfile = $this->getValue('dbpwdfile');
            $dbfile = $this->getValue('dbfile');
            $admin_username = $this->getValue('admin_username');
            $admin_password = $this->getValue('admin_password');
            $installer = new Installer(
                $baseurl,
                $socnettitle,
                $language,
                $dbtype,
                $dbhost,
                $dbname,
                $dbuser,
                $dbpwdfile,
                $dbfile
            );
            $installer->setAdminCredentials($admin_username, $admin_password);
            $validation_status = $installer->validate();
            switch ($validation_status) {
                case Installer::STATUS_OK:
                    if ($installer->save()) {
                        // Saved successfull
                        if (!$installer->initializeDatabase()) {
                            // Could not init the database tables
                            $messages = $installer->getMessages();
                            $this->addMessage(
                                array_pop($messages),
                                AlertMessage::PRIORITY_DANGER
                            );
                            break;
                        }
                        $admin_creation = AccountManager::getInstance()->signUp(
                            $admin_username,
                            $admin_password,
                            $admin_username .'@localhost',
                            '',
                            ''
                        );
                        if (!$admin_creation) {
                            // Could not create administrative account
                            $message = gettext("Could not insert administrative account in database");
                            $messages = $installer->getMessages();
                            if (count($messages) > 0) {
                                $message = array_pop($messages);
                            }
                            $this->addMessage(
                                $message,
                                AlertMessage::PRIORITY_DANGER
                            );
                            break;
                        }
                        // Everything went well
                        $this->addMessage(
                            gettext("Saved new configuration and prepared the database successfully"),
                            AlertMessage::PRIORITY_SUCCESS
                        );
                    } else {
                        // Could not save the data
                        $this->addMessage(
                            gettext("Could not save your settings"),
                            AlertMessage::PRIORITY_DANGER
                        );
                    }
                    break;
                case Installer::STATUS_INVALID_DBTYPE:
                    $this->addMessage(
                        gettext("Invalid database type selected"),
                        AlertMessage::PRIORITY_DANGER
                    );
                    break;
                case Installer::STATUS_INVALID_DBFILE:
                    $this->addMessage(
                        gettext("Database file not accessable"),
                        AlertMessage::PRIORITY_DANGER
                    );
                    break;
                case Installer::STATUS_INVALID_DBPASSWORDFILE:
                    $this->addMessage(
                        gettext("Database password file not accessable"),
                        AlertMessage::PRIORITY_DANGER
                    );
                    break;
                case Installer::STATUS_INVALID_LANGUAGE:
                    $this->addMessage(
                        gettext("Invalid language selected"),
                        AlertMessage::PRIORITY_DANGER
                    );
                    break;
                case Installer::STATUS_INVALID_TITLE:
                    $this->addMessage(
                        gettext("The title has to be between 3 and 50 characters"),
                        AlertMessage::PRIORITY_DANGER
                    );
                    break;
                case Installer::STATUS_INVALUD_BASEURL:
                    $this->addMessage(
                        gettext("The base URL seems to be to short"),
                        AlertMessage::PRIORITY_DANGER
                    );
                    break;
            }
            $this->addSpecialContent('TEXT', $this->messages);
        }
    }

    public function setup()
    {
        if (!empty($this->getValue(self::KEY_SUBMIT))) {
            $this->mode = self::MODE_SAVE;
        }
        if ($this->mode === self::MODE_FORMULAR) {
            $insertions = array(
                'INSTALLATION'        => gettext("Installation"),
                'INSTALLATION_LONG'   => gettext("Setup your own social network"),
                'INSTALLATION_TEXT'   => gettext("Before you can start using your own social network we have to discuss some mandatory things. But we will try to keep it short."),
                'SELECT_OPTION'       => gettext("Please select"),
                'NAME'                => gettext("Name"),
                'NAME_NOTE'           => gettext("How do you want to call this social network?"),
                'LANGUAGE'            => gettext("Language"),
                'LANGUAGE_NOTE'       => gettext("Which is the primary language?"),
                'DATABASE'            => gettext("Database"),
                'DBTYPE'              => gettext("Database system"),
                'DBTYPE_NOTE'         => gettext("Which database system will you use?"),
                'DBHOST'              => gettext("Host"),
                'DBHOST_NOTE'         => gettext("Which server should be used?"),
                'DBNAME'              => gettext("Name"),
                'DBNAME_NOTE'         => gettext("What is the name of the database?"),
                'DBUSER'              => gettext("User"),
                'DBUSER_NOTE'         => gettext("What is the database user name?"),
                'DBPASSWORDFILE'      => gettext("Password file"),
                'DBPASSWORDFILE_NOTE' => gettext("In which file is the users password stored in your file system?"),
                'DBFILE'              => gettext("File"),
                'DBFILE_NOTE'         => gettext("Where is the database file located?"),
                'SUBMIT'              => gettext("Submit"),
                'SUBMIT_NAME'         => self::KEY_SUBMIT,
                'DBTYPE_SQLITE'       => PDOConnector::DBTYPE_SQLITE,
                'DBTYPE_MYSQL'        => PDOConnector::DBTYPE_MYSQL,
                'DBTYPE_POSTGRES'     => PDOConnector::DBTYPE_POSTGRES,
                'ADMINISTRATOR'       => gettext("Administrator"),
                'USERNAME'            => gettext("Username"),
                'USERNAME_NOTE'       => gettext("username of the administrative account"),
                'PASSWORD'            => gettext("Password"),
                'PASSWORD_NOTE'       => gettext("Password to protect the administrative account"),
                'BASEURL'             => $this->getValue('baseurl')
            );
        } else {
            $insertions = array();
            $this->setTemplateName('general.html');
        }
        foreach ($insertions AS $key => $value) {
            $this->addSpecialContent($key, $value);
        }
    }

    protected function addMessage(string $message, string $priority = AlertMessage::PRIORITY_INFO)
    {
        $this->messages .= new AlertMessage($message, $priority);
    }

}
