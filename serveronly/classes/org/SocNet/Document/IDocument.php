<?php

namespace org\SocNet\Document;

interface IDocument
{

    /**
     * Returns a value from the formular data (POST) or an empty
     * string if no value was set.
     * @param string $key Key of the requested value
     * @return string Form data value
     */
    public function getValue(string $key): string;

    /**
     * Sets up the document regardless of the execution. Things like simple
     * translations for the template should be done here.
     */
    public function setup();

    /**
     * Executed the primary task of this document. This may produce Content
     * which can be returned by calling the getContent() method later.
     */
    public function execute();

    /**
     * Returns the previous generated Content.
     * @return string Content of the document
     */
    public function getContent(): string;

}