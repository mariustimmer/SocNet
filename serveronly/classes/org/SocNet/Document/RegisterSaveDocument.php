<?php

namespace org\SocNet\Document;

use \org\SocNet\Document\Element\AlertMessage AS AlertMessage;
use \org\SocNet\Controller\AccountManager AS AccountManager;
use \Exception AS Exception;

class RegisterSaveDocument extends AbstractDocument
{

    public function __construct()
    {
        parent::__construct('general.html');
        $this->setTitle(gettext("Sign up"));
    }

    public function execute()
    {
        $message = null;
        try {
            if (!isset($_POST[RegisterDocument::KEY_SUBMIT])) {
                /**
                 * The submit button was not pressed. This should never happen
                 * because it means that the script was called directly and not
                 * using the formular.
                 */
                throw new Exception(
                    gettext("You did not came here using the formular!")
                );
            }
            $firstname     = $this->getValue(RegisterDocument::KEY_FIRSTNAME);
            $lastname      = $this->getValue(RegisterDocument::KEY_LASTNAME);
            $username      = $this->getValue(RegisterDocument::KEY_USERNAME);
            $email         = $this->getValue(RegisterDocument::KEY_EMAIL);
            $email_confirm = $this->getValue(RegisterDocument::KEY_EMAIL_CONFIRM);
            $password      = $this->getValue(RegisterDocument::KEY_PASSWORD);
            $tac           = $this->getValue(RegisterDocument::KEY_TAC);
            if (empty($firstname)) {
                throw new Exception(gettext("We need your first name."));
            }
            if (empty($lastname)) {
                throw new Exception(gettext("We need your last name."));
            }
            if (empty($username)) {
                throw new Exception(gettext("We need to know what your username should be."));
            }
            if (empty($email)) {
                throw new Exception(gettext("We need your mail address."));
            }
            if (empty($email_confirm)) {
                throw new Exception(gettext("We need your mail address twice so we can check that you did make any mistake."));
            }
            if (strcasecmp($email, $email_confirm) !== 0) {
                throw new Exception(gettext("The mail addresses you gave us were not equeal"));
            }
            if (empty($password)) {
                throw new Exception(gettext("We need to know the password you want to use."));
            }
            if (empty($tac)) {
                throw new Exception(gettext("You have to agree to our terms and conditions first."));
            }
            $signin_status = AccountManager::getInstance()->signUp(
                $username,
                $password,
                $email,
                $firstname,
                $lastname
            );
            if ($signin_status === false) {
                $messages = AccountManager::getInstance()->getMessages();
                throw new Exception(array_pop($messages));
            }
            $message = new AlertMessage(
                gettext("Thank you. You should receive an mail within the next minutes. Just click on the link in it to verify and create your final account."),
                AlertMessage::PRIORITY_SUCCESS
            );
        } catch (Exception $exception) {
            $message = new AlertMessage(
                $exception->getMessage(),
                AlertMessage::PRIORITY_DANGER
            );
        }
        $this->addSpecialContent(
            'TEXT',
            $message
        );
    }

    public function setup()
    {
        $this->addSpecialContent('SUBTITLE', gettext("Your new account"));
    }

}
