<?php

namespace org\SocNet\Document;

use \org\SocNet\Runtime AS Runtime;
use \org\SocNet\Configuration AS Configuration;
use \org\SocNet\SessionManager AS SessionManager;
use \Exception AS Exception;

class Navigation
{

    /**
     * Navigation data as array from navigation.json
     * @var array
     */
    private $items;

    /**
     *Name of the current document class
     * @var string
     */
    private $current_documentclass;

    public function __construct(string $current_documentclass)
    {
        $this->items = $this->loadMenu();
        $this->current_documentclass = $current_documentclass;
    }

    private function loadMenu(): array
    {
        $filename = implode(
            DIRECTORY_SEPARATOR,
            array(
                DIRECTORY_SERVERONLY,
                'var',
                'navigation.json'
            )
        );
        try {
            if (!is_readable($filename)) {
                throw new Exception(
                    sprintf(
                        'Coult not read navigation file "%s"',
                        $filename
                    )
                );
            }
            $raw = file_get_contents($filename);
            if ($raw === false) {
                throw new Exception('No navigation content read');
            }
            $json = json_decode($raw, true);
            if ($json === false) {
                throw new Exception('No valid JSON in navigation file');
            }
            return $json;
        } catch (Exception $exception) {
            trigger_error($exception->getMessage());
            return array();
        }
    }

    public function __toString()
    {
        $output = '';
        $base_url = Configuration::get('general', 'baseurl');
        foreach ($this->items AS $documentClassName => $options) {
            if ((!Runtime::getSessionManager()->isLoggedIn()) &&
                ($options['public'] == 0)) {
                /**
                 * The user is not logged in and the document is not
                 * public so we skip this document in the menu
                 */
                continue;
            }
            $class = '';
            $current_documentclass_array = explode(
                chr(92),
                $this->current_documentclass
            );
            $documentclass_name_array = explode(
                chr(92),
                $documentClassName
            );
            if (end($current_documentclass_array) == end($documentclass_name_array)) {
                // This is the current document and needs to active
                $class .= 'active';
            }
            $url = '/';
            if (isset($options['url'])) {
                $url = $options['url'];
            }
            $url = $base_url . $url;
            $icon = '';
            if (isset($options['icon'])) {
                $icon = $options['icon'];
            }
            $name = '';
            if (isset($options['name'])) {
                $name = $options['name'];
            }
            $output .= sprintf(
                '<li class="%s"><a href="%s"><i class="now-ui-icons %s"></i><p>%s</p></a></li>',
                $class,
                $url,
                $icon,
                $name
            );
        }
        return '<ul class="nav">'. $output .'</ul>';
    }

}
