<?php

namespace org\SocNet\Document;

use \org\SocNet\Document\RestrictedDocument AS RestrictedDocument;
use \org\SocNet\Runtime AS Runtime;
use \org\SocNet\Document\Element\AlertMessage AS AlertMessage;

class HomeDocument extends RestrictedDocument
{

    public function __construct()
    {
        parent::__construct('home.html');
        $this->setTitle(gettext("Home"));
    }

    public function setup()
    {
        if (Runtime::getSessionManager()->isLoggedIn()) {
            /**
             * We do not need this special contents set when the user is not
             * logged in. In fact, we can not do this when there is no user
             * information. In this case the default restricted message will
             * be shown anyway.
             */
            $user = Runtime::getSessionManager()->getCurrentUser();
            if ($user !== null) {
                $this->addSpecialContent('DASHBOARD_SUBTITLE', sprintf(gettext("Welcome %s!"), Runtime::getSessionManager()->getCurrentUser()->getUsername()));
            } else {
                /**
                 * This should never happen, but just to be sure and
                 * safe we will catch this case.
                 */
                $this->addSpecialContent('DASHBOARD_SUBTITLE', new AlertMessage(gettext("Could not get any information about you"), AlertMessage::PRIORITY_DANGER));
            }
            $this->addSpecialContent('DASHBOARD', gettext("Dashboard"));
            $this->addSpecialContent('DASHBOARD_TEXT', gettext("This is your personal dashboard where you have a nice overview over new notifications such as messages or mentions."));
        }
    }

    public function execute()
    {

    }

}
