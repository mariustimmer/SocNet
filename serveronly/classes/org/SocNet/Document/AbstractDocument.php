<?php

namespace org\SocNet\Document;

use \org\SocNet\Configuration AS Configuration;

abstract class AbstractDocument implements IDocument
{

    const POST = INPUT_POST;
    const GET  = INPUT_GET;

    /**
     * Content of the document
     * @var string
     */
    private $content;

    /**
     * Name of the template to use
     * @var string
     */
    private $templatename;

    /**
     * Title of the document
     * @var string
     */
    private $title;

    /**
     * Associative array storing special replacements for the template
     * @var array
     */
    private $template_replacements;

    public function __construct(string $templatename)
    {
        $this->content = '';
        $this->setTemplateName($templatename);
        $this->template_replacements = array();
        $this->setTitle('');
    }

    protected function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets the name of the template to use.
     * @param string $templatename
     */
    protected function setTemplateName(string $templatename)
    {
        $this->templatename = $templatename;
    }

    public function getTemplateName(): string
    {
        return $this->templatename;
    }

    public function getTemplate(): string
    {
        $templatename = $this->getTemplateName();
        $template_directory = implode(
            DIRECTORY_SEPARATOR,
            array(
                DIRECTORY_SERVERONLY,
                'var',
                'templates'
            )
        );
        $template_filename = $template_directory . DIRECTORY_SEPARATOR . $templatename;
        if (!is_readable($template_filename)) {
            $template_filename = $template_directory . DIRECTORY_SEPARATOR .'default.html';
        }
        if (is_readable($template_filename)) {
            return file_get_contents($template_filename);
        }
        return '<!-- BODY -->';
    }

    /**
     * Use this method to add new content to this document.
     * @param string $element
     */
    protected function addContent(string $element)
    {
        $this->content .= $element;
    }

    protected function addSpecialContent(string $needle, string $replacement)
    {
        $this->template_replacements[$needle] = $replacement;
    }

    public function getContent(): string
    {
        $navigation_helper = new Navigation(get_class($this));
        $template = $this->getTemplate();
        $replacements = array_merge(
            array(
                'TITLE'             => $this->getTitle(),
                'BODY'              => $this->content,
                'MAINNAVIGATION'    => $navigation_helper,
                'DOCUMENT_LANGUAGE' => Configuration::get('general', 'language'),
                'INSTANCENAME'      => Configuration::get('general', 'instancename'),
                'BASEURL'           => Configuration::get('general', 'baseurl'),
                'HEADERS'           => ''
            ),
            $this->template_replacements
        );
        foreach ($replacements AS $needle => $replacement) {
            $template = str_replace(
                '<!-- '. $needle .' -->',
                $replacement,
                $template
            );
        }
        return $template;
    }

    public function getValue(string $key, string $input = self::POST): string
    {
        $value = filter_input(
            $input,
            $key,
            FILTER_SANITIZE_SPECIAL_CHARS
        );
        if (!$value) {
            $value = '';
        }
        return $value;
    }

}
