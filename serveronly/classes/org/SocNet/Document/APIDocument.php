<?php

namespace org\SocNet\Document;

use \org\SocNet\Request AS Request;
use \org\SocNet\API\Command\Secure\UserGetCommand AS UserGetCommand;

class APIDocument implements IDocument
{

    const KEY_STATUS    = 'status';
    const KEY_MESSAGES  = 'messages';
    const KEY_COMMAND   = 'command';
    const KEY_PARAMETER = 'parameter';
    const KEY_DATA      = 'data';
    const KEY_USERID    = 'uid';
    const STATUS_OKAY       = 1;
    const STATUS_ERROR      = 0;
    const STATUS_PERMISSION = 2;
    const COMMAND_USERGET = 'userget';

    private $messages;

    private $status;

    private $command;

    private $parameter;

    private $data;

    protected function addMessage(string $message): void
    {
        array_push($this->messages, $message);
    }

    /**
     * Reads out the requested value depending from the used request method.
     * @param string $key The requested variable
     * @return string The requested value
     */
    public function getValue(string $key): string
    {
        switch (Request::getMethod()) {
            case Request::METHOD_GET:
                return filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS) ?: '';
            case Request::METHOD_POST:
                return filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS) ?: '';
        }
        $data = trim(file_get_contents('php://input'));
        if (empty($data)) {
            $this->addMessage(gettext("No parameters available"));
            return '';
        }
        $json = json_decode($data);
        if (!$json) {
            $this->addMessage(gettext("Parameter is not valid JSON"));
            return '';
        }
        if (!isset($json[$key])) {
            $this->addMessage(sprintf(gettext("Parameter '%s' not set"), $key));
            return '';
        }
        return $json[$key];
    }

    /**
     * Reads out a parameter (using getValue()) and stores it in the parameter array if set.
     * @param string $key Variable to use
     * @return string|null Requested value or null if not set
     */
    protected function getParameter(string $key)
    {
        if (isset($this->parameter[$key])) {
            return $this->parameter[$key];
        }
        $value = $this->getValue($key);
        if ((empty(trim($value))) &&
            (trim($value) != '0')) {
            $this->addMessage(sprintf(gettext("Parameter '%s' is not set"), $key));
            return null;
        }
        $this->parameter[$key] = $value;
        return $this->parameter[$key];
    }

    public function setup()
    {
        header('Content-Type: application/json; Charset=UTF-8');
        $this->data      = [];
        $this->messages  = [];
        $this->command   = $this->getValue(self::KEY_COMMAND);
        $this->status    = self::STATUS_OKAY;
        $this->parameter = [];
    }

    public function execute()
    {
        $command = null;
        switch ($this->command) {
            case self::COMMAND_USERGET:
                $command = new UserGetCommand();
                break;
            default:
                $this->status = self::STATUS_ERROR;
                $this->addMessage(sprintf(gettext("Command '%s' is not implemented"), $this->command));
        }
        if ($command === null) {
            return;
        }
        foreach ($command::getParameterKeys() AS $key) {
            $value = $this->getParameter($key);
            if ($value) {
                $command->addParameter($key, $value);
            }
        }
        $command->execute();
        $this->data = $command->getData();
    }

    public function getContent(): string
    {
        return json_encode(
            [
                self::KEY_STATUS    => $this->status,
                self::KEY_MESSAGES  => $this->messages,
                self::KEY_COMMAND   => $this->command,
                self::KEY_PARAMETER => $this->parameter,
                self::KEY_DATA      => $this->data
            ]
        );
    }

}
