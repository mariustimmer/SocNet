<?php

namespace org\SocNet\Document;

use \org\SocNet\Document\RestrictedDocument AS RestrictedDocument;
use \org\SocNet\Runtime AS Runtime;

class LogoutDocument extends RestrictedDocument
{

    public function __construct()
    {
        parent::__construct('logout.html');
        $this->setTitle(gettext("Logout"));
    }

    public function setup()
    {
        $this->addSpecialContent('LOGOUT', gettext("Logout"));
        $this->addSpecialContent('LOGOUT_SUBTITLE', gettext("We hope to see you again soon"));
        $this->addSpecialContent('LOGOUT_TEXT', gettext("You logged out successfully. We and all your contacts hope to see you again soon."));
    }

    public function execute()
    {
        if (Runtime::getSessionManager()->isLoggedIn()) {
            Runtime::getSessionManager()->logout();
        }
    }

}
