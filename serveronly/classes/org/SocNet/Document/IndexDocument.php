<?php

namespace org\SocNet\Document;

use org\SocNet\Configuration;
use \org\SocNet\Document\AbstractDocument AS AbstractDocument;
use \org\SocNet\Document\Element\AlertMessage AS AlertMessage;
use \org\SocNet\Runtime AS Runtime;

class IndexDocument extends AbstractDocument
{

    const KEY_USERNAME     = 'username';
    const KEY_PASSWORD     = 'password';
    const KEY_LOGIN_SUBMIT = 'loginsubmit';

    public function __construct()
    {
        parent::__construct('index.html');
        $this->setTitle(gettext("Start"));
    }

    public function setup()
    {
        if (Runtime::getSessionManager()->isLoggedIn()) {
            $this->redirectToHome();
        }
        $this->addSpecialContent('LOGIN', gettext("Login"));
        $this->addSpecialContent('LOGIN_LONG', gettext("Log in into you account"));
        $this->addSpecialContent('LOGIN_TEXT', gettext("To log in to your account you have to enter your username/password combination so we can identify you."));
        $this->addSpecialContent('REGISTER_TEXT', gettext("In case you do not already have an account, here you can create a new one"));
        $this->addSpecialContent('REGISTER', gettext("Sign up"));
        $this->addSpecialContent('USERNAME', gettext("Username"));
        $this->addSpecialContent('PASSWORD', gettext("Password"));
        $this->addSpecialContent('SUBMIT', gettext("Log in"));
        $this->addSpecialContent('KEY_USERNAME', self::KEY_USERNAME);
        $this->addSpecialContent('KEY_PASSWORD', self::KEY_PASSWORD);
        $this->addSpecialContent('SUBMIT_NAME', self::KEY_LOGIN_SUBMIT);
        $this->addSpecialContent('NOTIFICATION', '');
    }

    /**
     * Redirects to the home document. This is useful when the user already is logged in.
     */
    private function redirectToHome() {
        Runtime::redirect(Configuration::get('general', 'baseurl'). '/home/');
    }

    public function execute()
    {
        if ($this->getValue(self::KEY_LOGIN_SUBMIT)) {
            // The user wants us to log him in
            $username = $this->getValue(self::KEY_USERNAME);
            $password = $this->getValue(self::KEY_PASSWORD);
            $success = Runtime::getSessionManager()->login($username, $password);
            if ($success) {
                /**
                 * The credentials were correct and the user id is set in
                 * the session now.
                 */
                $this->addSpecialContent(
                    'NOTIFICATION',
                    new AlertMessage(
                        gettext("Logged in successfully - Welcome"),
                        AlertMessage::PRIORITY_SUCCESS
                    )
                );
                $this->redirectToHome();
            } else {
                /**
                 * Wrong user credentials so we only print out
                 * a notification with that information.
                 */
                $this->addSpecialContent(
                    'NOTIFICATION',
                    new AlertMessage(
                        gettext("The user credentials you entered were wrong."),
                        AlertMessage::PRIORITY_DANGER
                    )
                );
            }
        }
    }

}
