<?php

namespace org\SocNet\Document\Element;

class AlertMessage
{

    const PRIORITY_SUCCESS = 'alert-success';
    const PRIORITY_INFO    = 'alert-info';
    const PRIORITY_WARNING = 'alert-warning';
    const PRIORITY_DANGER  = 'alert-danger';

    /**
     * Message to print out
     * @var string
     */
    private $message;

    /**
     * Priority of this message
     * @var string
     */
    private $priority;

    public function __construct(string $message, string $priority = self::PRIORITY_INFO)
    {
        $this->setMessage($message);
        $this->setPriority($priority);
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    public function setPriority(string $priority)
    {
        $this->priority = $priority;
    }

    public function __toString()
    {
        return sprintf(
            '<div class="alert %s" role="alert">%s</div>',
            $this->priority,
            $this->message
        );
    }

}