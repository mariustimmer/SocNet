<?php

namespace org\SocNet\Document;

use \org\SocNet\Document\AbstractDocument AS AbstractDocument;
use \org\SocNet\Configuration AS Configuration;

class ImpressumDocument extends AbstractDocument
{

    public function __construct()
    {
        parent::__construct('impressum.html');
        $this->setTitle(gettext("Impressum"));
    }

    public function setup()
    {
        $this->addSpecialContent('IMPRESSUM', gettext("Impressum"));
        $this->addSpecialContent('IMPRESSUM_LONG', gettext("Responsibilities"));
        $this->addSpecialContent('IMPRESSUM_TEXT', gettext("Responsible for this instance of the social network project is"));
        $this->addSpecialContent('IMPRESSUM_NAME', Configuration::get("impressum", "general"));
        $this->addSpecialContent('IMPRESSUM_ADDRESS', Configuration::get("impressum", "general_address"));
        $this->addSpecialContent('IMPRESSUM_TEC', gettext("Technical responsibe"));
        $this->addSpecialContent('IMPRESSUM_TEC_TEXT', gettext("Responsible for the technical part is"));
        $this->addSpecialContent('IMPRESSUM_TEC_ADDRESS', Configuration::get("impressum", "tec_address"));
        $this->addSpecialContent('IMPRESSUM_TEC_NAME', Configuration::get("impressum", "tec"));
        $this->addSpecialContent('IMPRESSUM_SOFTWARE', gettext("Software responsibility"));
        $this->addSpecialContent('IMPRESSUM_SOFTWARE_TEXT', gettext("Responsible for everything that comes with the software itself is"));
        $this->addSpecialContent('IMPRESSUM_SOFTWARE_URL', Configuration::get("impressum", "software_url"));
        $this->addSpecialContent('IMPRESSUM_SOFTWARE_NAME', Configuration::get("impressum", "software"));
    }

    public function execute()
    {
    }

}
