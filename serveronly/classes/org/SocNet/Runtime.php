<?php

namespace org\SocNet;

use \org\SocNet\Database\PDOConnector AS PDOConnector;
use \org\SocNet\Document\IDocument AS IDocument;
use \org\SocNet\SessionManager AS SessionManager;
use Exception AS Exception;

class Runtime
{

    /**
     * The static session manager.
     * @var SessionManager
     */
    private static $sessionmanager;

    /**
     * Database connector.
     * @var PDOConnector
     */
    private static $database_connector;

    /**
     * Returns the session manager or creates it if not available.
     * @return SessionManager
     */
    public static function getSessionManager(): SessionManager
    {
        if (is_null(self::$sessionmanager)) {
            self::$sessionmanager = new SessionManager();
        }
        return self::$sessionmanager;
    }

    /**
     * Returns the database connector or creates it if not available.
     * @return \PDOConnector
     */
    public static function getDatabaseConnector(): PDOConnector
    {
        if (is_null(self::$database_connector)) {
            self::$database_connector = new PDOConnector();
        }
        return self::$database_connector;
    }

    /**
     * Just writes the location header and does a temporary redirect.
     * @param string $url
     * @return void
     */
    public static function redirect(string $url): void {
        header(sprintf('Location: %s', $url), true, 307);
    }

    /**
     * Runs the requested document.
     * @param string $documentClass Name of a document class implementing IDocument
     * @return boolean
     */
    public function run(string $documentClass)
    {
        SessionManager::initLocale(
            substr(
                $_SERVER['HTTP_ACCEPT_LANGUAGE'],
                0,
                2
            )
        );
        $implements_interface = in_array(
            \org\SocNet\Document\IDocument::class,
            class_implements(
                $documentClass
            )
        );
        try {
            if (!$implements_interface) {
                /**
                 * Since the given class name is not a valid document class
                 * implementing our IDocument we can not be sure to know how
                 * to handle it and we do not want to execute unwanted code
                 * here. So we have to throw an exception.
                 */
                throw new Exception(
                    sprintf(
                        'Class %s does not implement IDocument',
                        $documentClass
                    )
                );
            }
            /* @var $document IDocument */
            $document = new $documentClass();
            $document->setup();
            $document->execute();
            print $document->getContent();
        } catch (Exception $exception) {
            /**
             * This general catch clausel is meant to avoid that the user
             * may see any information in error messages he should not see.
             */
            trigger_error($exception->getMessage());
            header(
                'HTTP/1.0 503 Service Unavailable',
                true,
                503
            );
            die('Could not process the request');
        }
        return true;
    }

}
