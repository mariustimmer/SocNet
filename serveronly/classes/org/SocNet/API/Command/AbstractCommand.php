<?php

namespace org\SocNet\API\Command;

abstract class AbstractCommand
{

    private $parameters;

    protected function __construct() {
        $this->parameters = [];
    }

    /**
     * Decides weather this command is secure and may only be executed by logged in users or not.
     * @return bool True if only logged in users may execute this command or false
     */
    public static function isSecure(): bool {
        return false;
    }

    abstract public static function getParameterKeys(): array;

    public function addParameter(string $key, string $value): void
    {
        $this->parameters[$key] = $value;
    }

    protected function getParameter(string $key)
    {
        if (!isset($this->parameters[$key])) {
            return null;
        }
        return $this->parameters[$key];
    }

    abstract public function execute(): void;

    public function getData(): array {
        return [];
    }

}
