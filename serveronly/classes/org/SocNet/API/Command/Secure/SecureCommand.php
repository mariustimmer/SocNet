<?php

namespace org\SocNet\API\Command\Secure;

use \org\SocNet\API\Command\AbstractCommand AS AbstractCommand;

abstract class SecureCommand extends AbstractCommand
{

    protected function __construct() {
        parent::__construct();
    }

    public static function isSecure(): bool {
        return true;
    }

}
