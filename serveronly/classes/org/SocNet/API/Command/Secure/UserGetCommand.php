<?php

namespace org\SocNet\API\Command\Secure;

use \org\SocNet\Database\DAO\UserDAO AS UserDAO;
use \org\SocNet\Database\DTO\User AS User;
use \org\SocNet\API\Command\Secure\SecureCommand AS SecureCommand;

class UserGetCommand extends SecureCommand
{

    const KEY_USERID = 'uid';

    /**
     * @var User
     */
    private $user_object;

    public function __construct() {
        parent::__construct();
        $this->user_object = null;
    }

    public static function getParameterKeys(): array
    {
        return [
            self::KEY_USERID
        ];
    }

    public function execute(): void
    {
        if ($this->getParameter(self::KEY_USERID)) {
            $results = User::DAO()->select(
                [
                    UserDAO::COLUMN_USERID => $this->getParameter(self::KEY_USERID)
                ]
            );
            if ((is_array($results)) &&
                (count($results) === 1)) {
                $this->user_object = $results[0];
            }
        }
    }

    public function getData(): array
    {
        if (is_null($this->user_object)) {
            return [];
        }
        return User::DAO()->toArray($this->user_object);
    }

}
