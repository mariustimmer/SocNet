<?php

if (!defined('DIRECTORY_SERVERONLY')) {
    // Just make sure the server only directory is available
    define('DIRECTORY_SERVERONLY', __DIR__);
}

$base_autoloader = function($classname) {
	$filename = sprintf(
		'%s%s%s%s%s.php',
		__DIR__,
		DIRECTORY_SEPARATOR,
		'classes',
		DIRECTORY_SEPARATOR,
		str_replace(
			chr(92),
			DIRECTORY_SEPARATOR,
			$classname
		)
	);
	if (is_readable($filename)) {
		require_once($filename);
		return TRUE;
	}
	return FALSE;
};

spl_autoload_register($base_autoloader);
