DROP TABLE IF EXISTS registrations;
DROP TABLE IF EXISTS profiledata;
DROP TABLE IF EXISTS usermails;
DROP TABLE IF EXISTS credentials;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    userid INTEGER PRIMARY KEY AUTOINCREMENT,
    username VARCHAR(32) NOT NULL,
    created TIMESTAMP NOT NULL,
    CONSTRAINT uc_users_username UNIQUE (username)
);
CREATE TABLE credentials (
    credentialid INTEGER PRIMARY KEY AUTOINCREMENT,
    userid INTEGER NOT NULL,
    passwordhash VARCHAR(64) NOT NULL,
    created TIMESTAMP NOT NULL,
    CONSTRAINT fk_credentials_users FOREIGN KEY (userid) REFERENCES users(userid)
);
CREATE TABLE usermails (
    usermailid INTEGER PRIMARY KEY AUTOINCREMENT,
    userid INTEGER,
    mailaddress VARCHAR(32) NOT NULL,
    created TIMESTAMP NOT NULL,
    CONSTRAINT fk_usermails_users FOREIGN KEY (userid) REFERENCES users(userid),
    CONSTRAINT uc_usermails_mailaddress UNIQUE (mailaddress)
);
CREATE TABLE profiledata (
    profileid INTEGER PRIMARY KEY AUTOINCREMENT,
    userid INTEGER,
    firstname VARCHAR(64) NOT NULL,
    lastname VARCHAR(64) NOT NULL,
    gender INTEGER DEFAULT 0,
    dayofbirth DATE NULL,
    created TIMESTAMP NOT NULL,
    CONSTRAINT fk_profiledata_users FOREIGN KEY (userid) REFERENCES users(userid)
);
CREATE TABLE registrations (
    regid INTEGER PRIMARY KEY AUTOINCREMENT,
    username VARCHAR(32) NOT NULL,
    firstname VARCHAR(64) NOT NULL,
    lastname VARCHAR(64) NOT NULL,
    mailaddress VARCHAR(32) NOT NULL,
    passwordhash VARCHAR(64) NOT NULL,
    verificationhash VARCHAR(64) NOT NULL,
    created TIMESTAMP NOT NULL
);