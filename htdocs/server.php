<?php

/**
 * Server connector
 * This script connects the htdocs part with the server only scripts and
 * classes. In case there is no information about the location of the
 * server only part there will only be a redirect to the installation.
 */

/**
 * This function will do a redirect to the installation and die. This is
 * useful in case there is no information about the server only directory
 *  because the system is not installed yet.
 */
function callInstallation()
{
	header('HTTP/1.1 302 Found');
	header('Location: install/');
	die();
}

/**
 * A file called server.dat containing the path to the server only directory
 * is expected next to this file. If it is not available it will be assumed
 * that the system is not installed yet and the user will be redirected to
 * the installation formular.
 */
if (!is_readable(__DIR__ . DIRECTORY_SEPARATOR .'server.dat')) {
	callInstallation();
}
$serverdirectory = trim(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .'server.dat'));

define('DIRECTORY_SERVERONLY', $serverdirectory);

if ((is_readable(DIRECTORY_SERVERONLY)) &&
    (is_readable(DIRECTORY_SERVERONLY . DIRECTORY_SEPARATOR .'autoload.php'))) {
	/**
	 * We can only continue if this directory as our dependency was found.
	 */
	require_once(DIRECTORY_SERVERONLY . DIRECTORY_SEPARATOR .'autoload.php');
} else {
	/**
	 * The server directory or the autoloader within it could not be read
	 * which is required. We can not continue without it and will redirect
	 * to the installation.
	 */
	callInstallation();
}
