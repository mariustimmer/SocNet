<?php

$pathfile = implode(
	DIRECTORY_SEPARATOR,
	array(
		__DIR__,
		'..',
		'server.dat'
	)
);

if ((!is_readable($pathfile)) &&
    (!empty(filter_input(INPUT_POST, 'initialdata')))) {
	/**
	 * There is no file containing the path to the server part but we
	 * have a path in the POST data which can be used.
	 */
	$serveronlydirectory = filter_input(
        INPUT_POST,
        'serveronlydirectory',
        FILTER_SANITIZE_SPECIAL_CHARS
    );
    $autoloader_path = $serveronlydirectory . DIRECTORY_SEPARATOR .'autoload.php';
    if ((!is_readable($serveronlydirectory)) ||
        (!is_readable($autoloader_path))) {
        /**
         * Something is wrong with the given server directory. In this case we
         * will print out a simple error message without any surrounding HTML.
         */
        print 'The given server directory does not exist or does not contain the "autoloader.php" file';
        die();
    }
    $written_bytes = file_put_contents($pathfile, $serveronlydirectory);
    if (($written_bytes === false) || ($written_bytes === 0)) {
        /**
         * Something went wrong writing the given and valid path to the path
         * containing file. Missing permissions might be the reason.
         */
        print 'Could not store server directory information! Is the htdocs directory writable for the webserver user?';
        die();
    }
}

/**
 * At this point we can be sure that the server directory connector file
 * was either written successfully or already exists so we can use it now.
 */
require_once(
    implode(
        DIRECTORY_SEPARATOR,
        array(
            __DIR__,
            '..',
            'server.php'
        )
    )
);

\org\SocNet\Runtime::run(
	\org\SocNet\Document\InstallDocument::class
);
