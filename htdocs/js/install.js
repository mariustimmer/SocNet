$(function() {
	$('#dbtype').change(function() {
		// Show only the required input fields
		var dbtype = $('#dbtype').val();
		$('fieldset.database .row').fadeIn();
		$('fieldset.database .row').not('.always').not('.'+ dbtype).fadeOut();
	});
	$('fieldset.database .row').not('.always').fadeOut(0);
});